package org.zxp.ConcurrentLatch;

public class Constants {
    /**单个线程池线程最大容量*/
    public static final int CURRENT_MAX_POOL_COUNT = 4;
    /**线程池最大数量*/
    public static final int MAX_EXCUTOR_COUNT = 3;
}
